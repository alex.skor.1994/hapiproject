
const Joi = require('joi');

const meta = Joi.object({
  total: Joi.number().integer().example(3)
});

const token = Joi.object({
  id: Joi.number().integer().example(1),
  token: Joi.string().example('4443655c28b42a4349809accb3f5bc71'),
  user_id: Joi.number().integer().example(2),
  expires_at: Joi.date().example('2019-02-16T15:38:48.243Z'),
  createdAt: Joi.date().example('2019-02-16T15:38:48.243Z'),
  updatedAt: Joi.date().example('2019-02-16T15:38:48.243Z')
});


const people = Joi.object({
  id: Joi.number().integer().example(1),
  name: Joi.string().required().example('Luke Skywalker'),
  height: Joi.number().integer().example(172),
  mass: Joi.number().integer().example(77),
  hair_color: Joi.string().required().example('blond'),
  skin_color: Joi.string().required().example('fair'),
  eye_color: Joi.string().required().example('blue'),
  birth_year: Joi.string().required().example('19BBY'),
  gender: Joi.string().required().example('male'),
  updatedAt: Joi.date().example('2019-02-16T15:38:48.243Z'),
  createdAt: Joi.date().example('2019-02-16T15:38:48.243Z')
});

const planets = Joi.object({
  id: Joi.number().integer().example(1),
  name: Joi.string().required().example('Luke Skywalker'),
  rotation_period: Joi.number().integer().example(172),
  orbital_period: Joi.number().integer().example(77),
  diameter: Joi.number().integer().example(77)
});


module.exports = {
  meta,
  token,
  people,
  planets
};
