const Joi = require('joi');
const axios = require('axios')

let BaseUrl = 'https://devnet-gate.decimalchain.com'

const Api = {
    getData(id) {
        return axios.create({
            withCredentials: true,
            baseURL: BaseUrl
        })
            .get(`/api/address/${id}/stakes`)
            .then((response) => {
                return response.data.result.validators;
            })
            .catch((error) => {
                alert(error);
            });
    }
}

async function response(request) {
    let data = []
    switch (request.params.network) {
        case 'devnet': {
            BaseUrl = 'https://devnet-gate.decimalchain.com'
            break
        }
        case 'testnet': {
            BaseUrl = 'https://testnet-gate.decimalchain.com'
            break
        }
        case 'mainnet': {
            BaseUrl = 'https://mainnet-gate.decimalchain.com'
        }
    }
    await Api.getData(request.params.id).then((response)=>{
            response.map((v)=>{
                data.push({
                    validatorId: v.validatorId,
                    stakeD: Number(v.totalStake)/1000000000000000000,
                    stakeV: Number(v.validator.stake)/1000000000000000000,
                    commissionV: Number(v.validator.fee)*100
                })
            })
    })
    return {
        data
    };
}


const responseScheme = Joi.object({
  data: Joi.array().items(
      Joi.object({
          validatorId: Joi.string().required().example('dxvaloper1mvqrrrlcd0gdt256jxg7n68e4neppu5tk872z3'),
          stakeD: Joi.number().required().example(120.12),
          commissionV: Joi.number().required().example(320.45),
          stakeV: Joi.number().required().example(10.1),
      })
  )
});

module.exports = {
    method: 'GET',
    path: '/data/{network}/{id}',
    options: {
        handler: response,
        tags: ['api'],
        validate: {
            params: {
                network: Joi.string().required().example('devnet'),
                id: Joi.string().required().example('dx1y4gwufx3n3cvwu22mcaqpjxda20s6d7330a5qj')
            }
        },
        response: { schema: responseScheme }
    }
};