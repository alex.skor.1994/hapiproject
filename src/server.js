const Hapi = require('@hapi/hapi');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const filepaths = require('filepaths');
const Sequelize = require('sequelize');
const HapiSwagger = require('hapi-swagger');
const AuthBearer = require('hapi-auth-bearer-token');
const hapiBoomDecorators = require('hapi-boom-decorators');
const Package = require('../package');
const config = require('../config');
const Logger = require('./libs/Logger');
const bearerValidation = require('./libs/bearerValidation');

const swaggerOptions = {
  info: {
    title: Package.name + ' API Documentation',
    description: Package.description
  },
  jsonPath: '/documentation.json',
  documentationPath: '/',
  schemes: ['https', 'http'],
  host: config.swaggerHost,
  debug: true
};

async function createServer(logLVL = config.logLVL) {
  const server = await new Hapi.Server(config.server);
  const logger = new Logger(logLVL, 'my-hapi-app');

  await server.register([
    AuthBearer,
    hapiBoomDecorators,
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: swaggerOptions
    },
    {
      plugin: require('hapi-sequelizejs'),
      options: [
        {
          name: config.db.database,
          models: [__dirname + '/models/*.js'],
          sequelize: new Sequelize(config.db),
          sync: true,
          forceSync: false,
        },
      ]
    }
  ]);


  server.ext({
    type: 'onRequest',
    method: async function (request, h) {
      request.server.config = Object.assign({}, config);
      request.server.logger = logger;
      return h.continue;
    }
  });


  server.auth.strategy('token', 'bearer-access-token', {
    allowQueryToken: false,
    unauthorized: bearerValidation.unauthorized,
    validate: bearerValidation.validate
  });

  server.ext('onPreResponse', function (request, h) {

    if (!request.response.isBoom) {
      return h.continue;
    }

    let responseObj = {
      message: request.response.output.statusCode === 401 ? 'AuthError' : 'ServerError',
      status: request.response.message
    };

    logger.error('code: ' + request.response.output.statusCode, request.response.message);
    return h.response(responseObj).code(request.response.output.statusCode);
  });

  let routes = filepaths.getSync(__dirname + '/routes/');
  for (let route of routes) {
    server.route(require(route.replace(__dirname + '\\', './').replace(__dirname + '/', './')));
  }

  try {
    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
  } catch (err) {
    console.log(JSON.stringify(err));
  }

  return server;
}

module.exports = createServer;
