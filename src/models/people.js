'use strict';

module.exports = (sequelize, DataTypes) => {
  const people = sequelize.define('people', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: {
    type: DataTypes.STRING,
        allowNull: false,
  },
    height: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    mass: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    hair_color: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    skin_color: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    eye_color: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    birth_year: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    gender: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  });

  people.dummyData = [
    {
      name: 'Luke Skywalker',
      height: 172,
      mass: 77,
      hair_color: 'blond',
      skin_color: 'fair',
      eye_color: 'blue',
      birth_year: '19BBY',
      gender: 'male',
    },
    {
      name: 'C-3PO',
      height: 167,
      mass: 75,
      hair_color: 'n/a',
      skin_color: 'gold',
      eye_color: 'yellow',
      birth_year: '112BBY',
      gender: 'n/a',
    },
    {
      name: 'R2-D2',
      height: 96,
      mass: 32,
      hair_color: 'n/a',
      skin_color: 'white, blue',
      eye_color: 'red',
      birth_year: '33BBY',
      gender: 'n/a',
    }
  ];

  return people;
};