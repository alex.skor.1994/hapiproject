'use strict';

module.exports = (sequelize, DataTypes) => {
    const planets = sequelize.define('planets', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        rotation_period: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        orbital_period: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        diameter: {
            type: DataTypes.INTEGER,
            allowNull: false,
        }
    });

    planets.dummyData = [
        {
            name: "Yavin IV",
            rotation_period: 24,
            orbital_period: 4818,
            diameter: 10200
        },
        {
            name: "Hoth",
            rotation_period: 23,
            orbital_period: 549,
            diameter: 7200
        }
    ];

    return planets;
};