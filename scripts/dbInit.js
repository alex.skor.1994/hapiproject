// console.log('DB init');

const fs = require('fs');
const Sequelize = require('sequelize');
const filepaths = require('filepaths');

const config = require('../config');

async function main() {
  if ( !fs.existsSync(__dirname + '/../db') ) {
    fs.mkdirSync(__dirname + '/../db');
  }

  let sequelize = new Sequelize(config.db);

  for (let modelFile of filepaths.getSync(__dirname + '/../src/models')) {
    require(modelFile.replace(__dirname + '/', ''))(sequelize, Sequelize.DataTypes);
  }

  await sequelize.sync();

  for (let tableName in sequelize.models) {
    let rowsCount = await sequelize.models[ tableName ].count();

    if ( rowsCount == 0 ) {
      console.log('>', tableName, 'rows count:', rowsCount, 'add dymmy data to the table...');
      sequelize.models[ tableName ].bulkCreate(sequelize.models[ tableName ].dummyData);
    } else {
      //await sequelize.sync({ force: true })

      console.log('>', tableName, 'rows count:', rowsCount, 'table data is already initialized');
    }
  }

  console.log('DB init DONE');
}

main();