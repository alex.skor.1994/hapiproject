
const Joi = require('joi');
const Boom = require('boom');

const responseSchemes = require('../../src/libs/responseSchemes');

async function response(request) {

  const accessTokens = request.getModel(request.server.config.db.database, 'access_tokens');
  const users = request.getModel(request.server.config.db.database, 'users');

  let userRecord = await users.findOne({ where: { email: request.query.login } });

  if ( !userRecord ) {
    throw Boom.unauthorized();
  }

  if ( !userRecord.verifyPassword(request.query.password) ) {
    throw Boom.unauthorized();
  }

  let token = await accessTokens.createAccessToken(userRecord);

  return {
    meta: {
      total: 1
    },
    data: [ token.dataValues ]
  };
}

const responseScheme = Joi.object({
  meta: responseSchemes.meta,
  data: Joi.array().items(responseSchemes.token)
});

const requestScheme =Joi.object({
  login: Joi.string().email().required().example('pupkin@gmail.com'),
  password: Joi.string().required().example('12345')
});

module.exports = {
  method: 'GET',
  path: '/auth',
  options: {
    handler: response,
    tags: [ 'api' ],
    validate: {
      query: requestScheme
    },
    response: { sample: 50, schema: responseScheme }
  }
};
