
const Boom = require('boom');

async function response(request) {
  
  throw Boom.badRequest();

  request.server.logger.error('request error', 'something went wrong');

  console.log(request.server.config);
  

  return {
    result: 'ok',
    message: 'Hello!'
  };
}

module.exports = {
  method: 'GET',
  path: '/',
  options: {
    handler: response
  }
};