
const Joi = require('joi');

const responseSchemes = require('../../src/libs/responseSchemes');

async function response(request) {

  const planets = request.getModel(request.server.config.db.database, 'planets');
  let newPlanets = await planets.create(request.payload);

  return {
    data: [ newPlanets.dataValues ]
  };
}

// Схема ответа
const responseScheme = Joi.object({
  data: Joi.array().items(responseSchemes.planets)
});

module.exports = {
  method: 'POST',
  path: '/planets/new',
  options: {
    handler: response,
    tags: ['api'], // Necessary tag for swagger
    validate: {
      payload: {
        name: Joi.string().min(1).max(100).required().example('Hoth'),
        rotation_period: Joi.number().integer().example(23),
        orbital_period: Joi.number().integer().example(549),
        diameter: Joi.number().integer().example(7200),
      }
    },
    response: { schema: responseScheme }

  }
};