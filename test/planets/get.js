
const Joi = require('joi');

const responseSchemes = require('../../src/libs/responseSchemes');

async function response(request) {

  const planets = request.getModel(request.server.config.db.database, 'planets');

  let data = await planets.findAll();
  let count = await planets.count();
  let result = [];
  for (let item of data) {
    result.push(item.get({ plain: true }));
  }
  
  return {
    meta: {
      total: count
    },
    data: result
  };
}

const responseScheme = Joi.object({
  meta: responseSchemes.meta,
  data: Joi.array().items(responseSchemes.planets)
});

module.exports = {
  method: 'GET',
  path: '/planets',
  options: {
    handler: response,
    tags: ['api'],
    response: { schema: responseScheme }
  }
};