
const Joi = require('joi');
const Boom = require('boom');

const responseSchemes = require('../../src/libs/responseSchemes');

async function response(request) {

  const people = request.getModel(request.server.config.db.database, 'people');
  let data = await people.findOne({ where: { id: request.params.id } });

  if ( !data ) {
    throw Boom.notFound();
  }


  return {
    data: [ data.get({ plain: true }) ]
  };
}

const responseScheme = Joi.object({
  data: Joi.array().items(responseSchemes.people)
});

module.exports = {
  method: 'GET',
  path: '/people/{id}',
  options: {
    handler: response,
    tags: ['api'],
    validate: {
      params: {
        id: Joi.number().integer().required().example(1)
      }
    },
    response: { schema: responseScheme }
  }
};