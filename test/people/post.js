
const Joi = require('joi');

const responseSchemes = require('../../src/libs/responseSchemes');

async function response(request) {

  const people = request.getModel(request.server.config.db.database, 'people');
  let newPeople = await people.create(request.payload);
  return {
    data: [ newPeople.dataValues ]
  };
}

// Схема ответа
const responseScheme = Joi.object({
  data: Joi.array().items(responseSchemes.people)
});

module.exports = {
  method: 'POST',
  path: '/people/new',
  options: {
    handler: response,
    tags: ['api'], // Necessary tag for swagger
    validate: {
      payload: {
        name: Joi.string().min(1).max(100).required().example('Luke Skywalker'),
        height: Joi.number().integer().example(172),
        mass: Joi.number().integer().example(77),
        hair_color: Joi.string().required().example('blond'),
        skin_color: Joi.string().required().example('fair'),
        eye_color: Joi.string().required().example('blue'),
        birth_year: Joi.string().required().example('19BBY'),
        gender: Joi.string().required().example('male'),
      }
    },
    response: { schema: responseScheme }

  }
};