
const Joi = require('joi');

const responseSchemes = require('../../src/libs/responseSchemes');

async function response(request) {

  const people = request.getModel(request.server.config.db.database, 'people');

  let data = await people.findAll();
  let count = await people.count();
  let result = [];
  for (let item of data) {
    result.push(item.get({ plain: true }));
  }
  
  return {
    meta: {
      total: count
    },
    data: result
  };
}

const responseScheme = Joi.object({
  meta: responseSchemes.meta,
  data: Joi.array().items(responseSchemes.people)
});

module.exports = {
  method: 'GET',
  path: '/people',
  options: {
    handler: response,
    tags: ['api'],
    response: { schema: responseScheme }
  }
};